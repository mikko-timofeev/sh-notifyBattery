#!/bin/sh
# Dependencies: upower, zenity

BATB=15 # battery % level considered as low

ONCE="${1:-false}"
while :; do
  BINF=`upower -i $(upower -e | grep BAT) | grep --color=never -E "state|percentage|icon-name"`
  PERC=`echo "$BINF" | grep percentage: | cut -c 26- | rev | cut -c 2- | rev`
  STAT=`echo "$BINF" | grep state: | cut -c 26-`

  NEXT=15
  if ([ "$STAT = discharging" ] && [ $PERC -le $BATB ]) || $ONCE; then
    ICON=`echo "$BINF" | grep icon-name: | cut -c 26- | rev | cut -c 2- | rev`
    LABEL="Battery: $PERC% [$STAT]"
    NEXT=5
    zenity --warning --icon-name="$ICON" --text="$LABEL"
  fi
  if $ONCE; then
    break
  fi
  sleep $NEXT
done
